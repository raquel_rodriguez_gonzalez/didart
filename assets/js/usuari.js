    // ---- VARIABLES GLOBALES ----
var app = angular.module('didart',["ngResource"]);

// ------ CONTROLADOR DE ANGULAR ----
app.controller('UsuarisController',function($scope,UsuarisFactory) {
    $scope.usuaris = [];

    // ---- GET -----

    UsuarisFactory.query(function(usuaris){
        $scope.usuaris = usuaris;
    });


    $scope.afegirUsuari = function() {
        if ($scope.nombre) {
            UsuarisFactory.save({
                usuario: $scope.usuario,
                contrasenya: $scope.contrasenya,
                email: $scope.email,
                nombre : $scope.nombre,
                fecha : $scope.fecha,
                tipo : $scope.tipo
            }, function(usuari) {
                $scope.usuaris.unshift(usuari);
                $scope.usuario = null;
                $scope.contrasenya = null;
                $scope.email = null;
                $scope.nombre = null;
                $scope.fecha = null;
                $scope.tipo = null;
            });
        }
    };

    $scope.editarUsuari = function(usuari) {
        $scope.editUsuario = usuari.usuario;
        $scope.editContrasenya = usuari.contrasenya;
        $scope.editEmail = usuari.email;
        $scope.editNombre = usuari.nombre;
        $scope.editFecha = usuari.fecha;
        $scope.editTipo = usuari.tipo;
        $scope.AutorAEditar = usuari;
    };

    $scope.updateUsuari = function() {
        if ($scope.editUsuario) {
            var UsuariAEditar = JSON.parse(JSON.stringify($scope.UsuariAEditar));
            console.log(UsuariAEditar);
            UsuariAEditar.usuario = $scope.editusuario;
            UsuariAEditar.contrasenya = $scope.editcontrasenya;
            UsuariAEditar.email = $scope.editemail;
            UsuariAEditar.nombre = $scope.editnombre;
            UsuariAEditar.fecha = $scope.editfecha;
            UsuariAEditar.tipo = $scope.edittipo;
            UsuariFactory.update(UsuariAEditar, function(e){
                $scope.UsuariAEditar.usuario =$scope.editusuario;
                $scope.UsuariAEditar.contrasenya =$scope.editcontrasenya;
                $scope.UsuariAEditar.email =$scope.editemail;
                $scope.UsuariAEditar.nombre =$scope.editnombre;
                $scope.UsuariAEditar.fecha =$scope.editfecha;
                $scope.UsuariAEditar.tipo =$scope.edittipo;
                $scope.editusuario = null;
                $scope.editcontrasenya = null;
                $scope.editemail = null;
                $scope.editnombre = null;
                $scope.editfecha = null;
                $scope.edittipo = null;
                delete $scope.UsuariAEditar;
            });
        }
    };
//REVISAR ID!!!
    $scope.eliminarUsuari = function(usuari) {
        console.log(usuari.nombre);
        UsuariFactory.delete({id:usuari.nombre},function(l){
            console.log($scope.usuaris.indexOf(usuari));
            $scope.usuaris.splice($scope.usuaris.indexOf(usuari),1);
        });
        console.log(usuari);
    };
});
// ---- RESOURCE ANGULAR -------
app.factory("UsuariFactory", function($resource) {
    return $resource("/api/usuaris/:id", null,
        {
            'update': { method:'PUT' }
        });
});
