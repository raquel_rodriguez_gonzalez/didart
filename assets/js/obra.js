    // ---- VARIABLES GLOBALES ----
var app = angular.module('didart',["ngResource"]);

// ------ CONTROLADOR DE ANGULAR ----
app.controller('ObresController',function($scope,ObresFactory) {
    $scope.obres = [];

    // ---- GET -----

    ObresFactory.query(function(obres){
        $scope.obres = obres;
    });


    $scope.afegirObra = function() {
        if ($scope.nombre) {
            ObresFactory.save({
                titulo: $scope.titulo,
                ano: $scope.ano,
                descripcion: $scope.descripcion,
                usuari_id: $scope.usuari_id,
                autor_id: $scope.autor_id,
                epoca_id: $scope.epoca_id
            }, function(obra) {
                $scope.obres.unshift(obra);
                $scope.titulo = null;
                $scope.ano = null;
                $scope.descripcion = null;
                $scope.usuari_id = null;
                $scope.autor_id = null;
                $scope.epoca_id = null;
                
            });
        }
    };

    $scope.editarObra = function(obra) {
        $scope.editTitulo = obra.titulo;
        $scope.editano = obra.ano;
        $scope.editdescripcion = obra.descripcion;
        $scope.editusuari_id = obra.usuari_id;
        $scope.editautor_id = obra.autor_id;
        $scope.editepoca_id = obra.epoca_id;
        $scope.ObraAEditar = obra;
    };

    $scope.updateObra = function() {
        if ($scope.editNombre) {
            var ObraAEditar = JSON.parse(JSON.stringify($scope.ObraAEditar));
            console.log(ObraAEditar);
            ObraAEditar.titulo = $scope.editTitulo;
            ObraAEditar.ano = $scope.editano;
            ObraAEditar.descripcion = $scope.editdescripcion;
            ObraAEditar.usuari_id = $scope.editusuari_id;
            ObraAEditar.autor_id = $scope.editautor_id;
            ObraAEditar.epoca_id = $scope.editepoca_id;
            ObraFactory.update(ObraAEditar, function(e){
                $scope.ObraAEditar.titulo =$scope.editTitulo;
                $scope.ObraAEditar.ano =$scope.editano;
                $scope.ObraAEditar.descripcion =$scope.editdescripcion;
                $scope.ObraAEditar.usuari_id =$scope.editusuari_id;
                $scope.ObraAEditar.autor_id =$scope.editautor_id;
                $scope.ObraAEditar.epoca_id =$scope.editepoca_id;
                $scope.editTitulo = null;
                $scope.editano = null;
                $scope.editdescripcion = null;
                $scope.editusuari_id = null;
                $scope.editautor_id = null;
                $scope.editepoca_id = null;
                delete $scope.ObraAEditar;
            });
        }
    };
//REVISAR ID!!!
    $scope.eliminarObra = function(obra) {
        console.log(obra.nombre);
        ObraFactory.delete({id:obra.nombre},function(l){
            console.log($scope.obres.indexOf(obra));
            $scope.obres.splice($scope.obres.indexOf(obra),1);
        });
        console.log(obra);
    };
});
// ---- RESOURCE ANGULAR -------
app.factory("ObresFactory", function($resource) {
    return $resource("/api/obres/:id", null,
        {
            'update': { method:'PUT' }
        });
});