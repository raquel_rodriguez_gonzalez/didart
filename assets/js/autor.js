    // ---- VARIABLES GLOBALES ----
var app = angular.module('didart',["ngResource"]);

// ------ CONTROLADOR DE ANGULAR ----
app.controller('AutorsController',function($scope,AutorsFactory) {
    $scope.autors = [];

    // ---- GET -----

    AutorsFactory.query(function(autors){
        $scope.autors = autors;
    });


    $scope.afegirAutor = function() {
        if ($scope.nombre) {
            AutorsFactory.save({
                nombre: $scope.nombre,
                foto: $scope.foto,
                nacionalidad: $scope.nacionalidad,
                lapso_vida : $scope.lapso_vida
            }, function(autor) {
                $scope.autors.unshift(autor);
                $scope.nombre = null;
                $scope.foto = null;
                $scope.nacionalidad = null;
                $scope.lapso_vida = null;
            });
        }
    };

    $scope.editarAutor = function(autor) {
        $scope.editNombre = autor.nombre;
        $scope.editFoto = autor.foto;
        $scope.editNacionalidad = autor.nacionalidad;
        $scope.editLapso_vida = autor.lapso_vida;
        $scope.AutorAEditar = autor;
    };

    $scope.updateAutor = function() {
        if ($scope.editNombre) {
            var AutorAEditar = JSON.parse(JSON.stringify($scope.AutorAEditar));
            console.log(AutorAEditar);
            AutorAEditar.nombre = $scope.editNombre;
            AutorAEditar.Foto = $scope.editFoto;
            AutorAEditar.Nacionalidad = $scope.editNacionalidad;
            AutorAEditar.lapso_vida = $scope.editLapso_vida;
            AutorFactory.update(AutorAEditar, function(e){
                $scope.AutorAEditar.nombre =$scope.editNombre;
                $scope.AutorAEditar.Foto =$scope.editFoto;
                $scope.AutorAEditar.Nacionalidad =$scope.editNacionalidad;
                $scope.AutorAEditar.lapso_vida =$scope.editLapso_vida;
                $scope.editNombre = null;
                $scope.editFoto = null;
                $scope.editNacionalidad = null;
                $scope.editLapso_vida = null;
                delete $scope.AutorAEditar;
            });
        }
    };
//REVISAR ID!!!
    $scope.eliminarAutor = function(autor) {
        console.log(autor.nombre);
        AutorFactory.delete({id:autor.nombre},function(l){
            console.log($scope.autors.indexOf(autor));
            $scope.autors.splice($scope.autors.indexOf(autor),1);
        });
        console.log(autor);
    };
});
// ---- RESOURCE ANGULAR -------
app.factory("AutorsFactory", function($resource) {
    return $resource("/api/autors/:id", null,
        {
            'update': { method:'PUT' }
        });
});
