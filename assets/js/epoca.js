    // ---- VARIABLES GLOBALES ----
var app = angular.module('didart',["ngResource"]);

// ------ CONTROLADOR DE ANGULAR ----
app.controller('EpoquesController',function($scope,EpoquesFactory) {
    $scope.epoques = [];

    // ---- GET -----

    EpoquesFactory.query(function(epoques){
        $scope.epoques = epoques;
    });


    $scope.afegirEpoca = function() {
        if ($scope.nombre) {
            EpoquesFactory.save({
                nombre: $scope.nombre,
                lapso_tiempo: $scope.lapso_tiempo,
            }, function(epoca) {
                $scope.epoques.unshift(epoca);
                $scope.nombre = null;
                $scope.lapso_tiempo = null;
            });
        }
    };

    $scope.editarEpoca = function(epoca) {
        $scope.editNombre = epoca.nombre;
        $scope.editlapso_tiempo = epoca.lapso_tiempo;
        $scope.EpocaAEditar = epoca;
    };

    $scope.updateEpoca = function() {
        if ($scope.editNombre) {
            var EpocaAEditar = JSON.parse(JSON.stringify($scope.EpocaAEditar));
            console.log(EpocaAEditar);
            EpocaAEditar.nombre = $scope.editNombre;
            EpocaAEditar.lapso_tiempo = $scope.editlapso_tiempo;
            EpocaFactory.update(EpocaAEditar, function(e){
                $scope.EpocaAEditar.nombre =$scope.editnombre;
                $scope.EpocaAEditar.lapso_tiempo =$scope.editlapso_tiempo;
                $scope.editnombre = null;
                $scope.editlapso_tiempo = null;
                delete $scope.EpocaAEditar;
            });
        }
    };
//REVISAR ID!!!
    $scope.eliminarEpoca = function(epoca) {
        console.log(epoca.nombre);
        EpocaFactory.delete({id:epoca.nombre},function(l){
            console.log($scope.epoques.indexOf(epoca));
            $scope.epoques.splice($scope.epoques.indexOf(epoca),1);
        });
        console.log(epoca);
    };
});
// ---- RESOURCE ANGULAR -------
app.factory("EpoquesFactory", function($resource) {
    return $resource("/api/epoques/:id", null,
        {
            'update': { method:'PUT' }
        });
});
