// ---- RESOURCE ANGULAR -------
angular.module('didart').factory("EpocaFactory", function($resource) {
    return $resource("/api/epoques/:id", null,
        {
            'update': { method:'PUT' }
        });
});