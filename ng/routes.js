
angular.module('didart')
    .config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            controller: "HomeController",
            templateUrl: "benvinguda.html"
    })
    .when("/home", {
            controller: "HomeController",
            templateUrl: "home.html"
    })
    //AUTORS
        .when("/autors", {
            controller: "AutorsController",
            templateUrl: "autor/LlistaAutor.html"
    })
        .when("/nouautor", {
            controller: "AutorsController",
            templateUrl: "autor/newAutor.html"
    })
        .when("/editautor", {
            controller: "AutorsController",
            templateUrl: "autor/delAutor.html"
    })
        .when("/delautor", {
            controller: "AutorsController",
            templateUrl: "autor/delAutor.html"
    })
    //OBRA
        .when("/obres", {
            controller: "ObraController",
            templateUrl: "obra/LlistaObra.html"
    })
        .when("/novaobra", {
            controller: "ObraController",
            templateUrl: "obra/newObra.html"
    })
        .when("/editobra", {
            controller: "ObraController",
            templateUrl: "obra/delObra.html"
    })
        .when("/delobra", {
            controller: "ObraController",
            templateUrl: "obra/delObra.html"
    })
    
    //EPOCA
        .when("/epoques", {
            controller: "EpocaController",
            templateUrl: "epoca/LlistaEpoca.html"
    })
        .when("/nouepoca", {
            controller: "EpocaController",
            templateUrl: "epoca/newEpoca.html"
    })
        .when("/editepoca", {
            controller: "EpocaController",
            templateUrl: "epoca/delEpoca.html"
    })
        .when("/delepoca", {
            controller: "EpocaController",
            templateUrl: "epoca/delEpoca.html"
    })
    
    //USUARI
        .when("/usuaris", {
            controller: "UsuariController",
            templateUrl: "usuari/LlistaUsuari.html"
    })
        .when("/nouusuari", {
            controller: "UsuariController",
            templateUrl: "usuari/newUsuari.html"
    })
        .when("/editusuari", {
            controller: "UsuariController",
            templateUrl: "usuari/delUsuari.html"
    })
        .when("/delusuari", {
            controller: "UsuariController",
            templateUrl: "usuari/delUsuari.html"
    })

        .when("/login", {
            controller: "LoginController",
            templateUrl: "login.html"
    });
    $locationProvider.html5Mode ({
        enabled: true,
        requireBase: false
    });
});
