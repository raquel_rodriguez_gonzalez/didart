// ---- RESOURCE ANGULAR -------
angular.module('didart').factory("AutorsFactory", function($resource) {
    return $resource("/api/autors/:id", null,
        {
            'update': { method:'PUT' }
        });
});