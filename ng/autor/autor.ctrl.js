// ------ CONTROLADOR DE ANGULAR ----



angular.module('didart').controller('AutorsController',function($scope,AutorsFactory,FileUploader, imatgesService) {
    $scope.autors = [];
        imatgesService.fetch()
        .success(function(imatges){
            $scope.imatges = imatges;
        });
    var uploader = $scope.uploader = new FileUploader({url:"/api/autors",alias:"foto",removeAfterUpload: true});
        uploader.onBeforeUploadItem = function (item) {
            item.formData.push({nombre: $scope.nombre});
            item.formData.push({nacionalidad: $scope.nacionalidad});
            item.formData.push({lapso_vida: $scope.lapso_vida});
        };
    // ---- GET -----

    AutorsFactory.query(function(autors){
        $scope.autors = autors;
    });


    $scope.afegirAutor = function() {
        
        if ($scope.nombre) {
            AutorsFactory.save({
                nombre: $scope.nombre,
                nacionalidad: $scope.nacionalidad,
                lapso_vida : $scope.lapso_vida,
                foto: $scope.foto
            }, function(autor) {
                $scope.autors.unshift(autor);
                $scope.nombre = null;
                $scope.nacionalidad = null;
                $scope.lapso_vida = null;
                $scope.foto = null;
            });
        }
    };

    $scope.editarAutor = function(autor) {
        $scope.editNombre = autor.nombre;
        $scope.editNacionalidad = autor.nacionalidad;
        $scope.editLapso_vida = autor.lapso_vida;
        $scope.editFoto = autor.foto;
        $scope.AutorAEditar = autor;
    };

    $scope.updateAutor = function() {
        if ($scope.editNombre) {
            var AutorAEditar = JSON.parse(JSON.stringify($scope.AutorAEditar));
            console.log(AutorAEditar);
            AutorAEditar.nombre = $scope.editNombre;
            AutorAEditar.Nacionalidad = $scope.editNacionalidad;
            AutorAEditar.lapso_vida = $scope.editLapso_vida;
            AutorAEditar.foto = $scope.editFoto;
            AutorsFactory.update(AutorAEditar, function(e){
                $scope.AutorAEditar.nombre =$scope.editNombre;
                $scope.AutorAEditar.Nacionalidad =$scope.editNacionalidad;
                $scope.AutorAEditar.lapso_vida =$scope.editLapso_vida;
                $scope.AutorAEditar.foto =$scope.editFoto;
                $scope.editNombre = null;
                $scope.editNacionalidad = null;
                $scope.editlapso_vida = null;
                $scope.editFoto = null;
                delete $scope.AutorAEditar;
            });
        }
    };
//REVISAR ID!!!
    $scope.eliminarAutor = function(autor) {
        console.log(autor._id);
        AutorsFactory.delete({id:autor._id},function(l){
            console.log($scope.autors.indexOf(autor));
            $scope.autors.splice($scope.autors.indexOf(autor),1);
        });
        console.log(autor);
    };
});