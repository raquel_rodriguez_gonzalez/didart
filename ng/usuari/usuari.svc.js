// ---- RESOURCE ANGULAR -------
angular.module('didart').factory("UsuariFactory", function($resource) {
    return $resource("/api/usuaris/:id", null,
        {
            'update': { method:'PUT' }
        });
});
