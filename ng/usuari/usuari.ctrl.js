// ------ CONTROLADOR DE ANGULAR ----
angular.module('didart').controller('UsuariController',function($scope,UsuariFactory) {
    $scope.usuaris = [];

    // ---- GET -----

    UsuariFactory.query(function(usuaris){
        $scope.usuaris = usuaris;
    });


    $scope.afegirUsuari = function() {
        if ($scope.usuario) {
            UsuariFactory.save({
                usuario: $scope.usuario,
                contrasenya: $scope.contrasenya,
                email: $scope.email,
                nombre : $scope.nombre,
                fecha : $scope.fecha,
                tipo : $scope.tipo
            }, function(usuari) {
                $scope.usuaris.unshift(usuari);
                $scope.usuario = null;
                $scope.contrasenya = null;
                $scope.email = null;
                $scope.nombre = null;
                $scope.fecha = null;
                $scope.tipo = null;
            });
        }
    };

    $scope.editarUsuari = function(usuari) {
        $scope.editusuario = usuari.usuario;
        $scope.editcontrasenya = usuari.contrasenya;
        $scope.editemail = usuari.email;
        $scope.editnombre = usuari.nombre;
        $scope.editfecha = usuari.fecha;
        $scope.editipo = usuari.tipo;
        $scope.UsuariAEditar = usuari;
    };

    $scope.updateUsuari = function() {
        if ($scope.editusuario) {
            var UsuariAEditar = JSON.parse(JSON.stringify($scope.UsuariAEditar));
            console.log(UsuariAEditar);
            UsuariAEditar.usuario = $scope.editusuario;
            UsuariAEditar.contrasenya = $scope.editcontrasenya;
            UsuariAEditar.email = $scope.editemail;
            UsuariAEditar.nombre = $scope.editnombre;
            UsuariAEditar.fecha = $scope.editfecha;
            UsuariAEditar.tipo = $scope.editipo;
            UsuariFactory.update(UsuariAEditar, function(e){
                $scope.UsuariAEditar.usuario =$scope.editusuario;
                $scope.UsuariAEditar.contrasenya =$scope.editcontrasenya;
                $scope.UsuariAEditar.email =$scope.editemail;
                $scope.UsuariAEditar.nombre =$scope.editnombre;
                $scope.UsuariAEditar.fecha =$scope.editfecha;
                $scope.UsuariAEditar.tipo =$scope.editipo;
                $scope.editusuario = null;
                $scope.editcontrasenya = null;
                $scope.editemail = null;
                $scope.editnombre = null;
                $scope.editfecha = null;
                $scope.editipo = null;
                delete $scope.UsuariAEditar;
            });
        }
    };
//REVISAR ID!!!
    $scope.eliminarUsuari = function(usuari) {
        console.log(usuari._id);
        UsuariFactory.delete({id:usuari._id},function(l){
            console.log($scope.usuaris.indexOf(usuari));
            $scope.usuaris.splice($scope.usuaris.indexOf(usuari),1);
        });
        console.log(usuari);
    };
});