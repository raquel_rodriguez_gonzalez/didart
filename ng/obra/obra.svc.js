// ---- RESOURCE ANGULAR -------
angular.module('didart').factory("ObraFactory", function($resource) {
    return $resource("/api/obres/:id", null,
        {
            'update': { method:'PUT' }
        });
});