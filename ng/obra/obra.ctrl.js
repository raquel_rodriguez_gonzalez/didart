// ------ CONTROLADOR DE ANGULAR ----
angular.module('didart').controller('ObraController',function($scope, ObraFactory, AutorsFactory, EpocaFactory, UsuariFactory, FileUploader, imatgesService) {
    $scope.obres = [];
    $scope.autors = [];
    $scope.usuaris = [];
    $scope.epoques = [];
            imatgesService.fetch()
        .success(function(imatges){
            $scope.imatges = imatges;
        });
    var uploader = $scope.uploader = new FileUploader({url:"/api/obres",alias:"foto",removeAfterUpload: true});
        uploader.onBeforeUploadItem = function (item) {
            item.formData.push({titulo: $scope.titulo});
            item.formData.push({ano: $scope.ano});
            item.formData.push({descripcion: $scope.descripcion});
            item.formData.push({usuari_id: $scope.usuari_id});
            item.formData.push({autor_id: $scope.autor_id});
            item.formData.push({epoca_id: $scope.epoca_id});
        };
    // ---- GET -----

    ObraFactory.query(function(obres){
        $scope.obres = obres;
    });
    
    AutorsFactory.query(function(autors){
        $scope.autors = autors;
    });
    
    UsuariFactory.query(function(usuaris){
        $scope.usuaris = usuaris;
    });
    
    EpocaFactory.query(function(epoques){
        $scope.epoques = epoques;
    });   

    $scope.afegirObra = function() {
        if ($scope.titulo) {
            ObraFactory.save({
                titulo: $scope.titulo,
                ano: $scope.ano,
                descripcion: $scope.descripcion,
                usuari_id: $scope.usuari_id,
                autor_id: $scope.autor_id,
                epoca_id: $scope.epoca_id
            }, function(obra) {
                $scope.obres.unshift(obra);
                $scope.titulo = null;
                $scope.ano = null;
                $scope.descripcion = null;
                $scope.usuari_id = null;
                $scope.autor_id = null;
                $scope.epoca_id = null;
                
            });
        }
    };

    $scope.editarObra = function(obra) {
        $scope.editTitulo = obra.titulo;
        $scope.editano = obra.ano;
        $scope.editdescripcion = obra.descripcion;
        $scope.editusuari_id = obra.usuari_id;
        $scope.editautor_id = obra.autor_id;
        $scope.editepoca_id = obra.epoca_id;
        $scope.ObraAEditar = obra;
    };

    $scope.updateObra = function() {
        if ($scope.editNombre) {
            var ObraAEditar = JSON.parse(JSON.stringify($scope.ObraAEditar));
            console.log(ObraAEditar);
            ObraAEditar.titulo = $scope.editTitulo;
            ObraAEditar.ano = $scope.editano;
            ObraAEditar.descripcion = $scope.editdescripcion;
            ObraAEditar.usuari_id = $scope.editusuari_id;
            ObraAEditar.autor_id = $scope.editautor_id;
            ObraAEditar.epoca_id = $scope.editepoca_id;
            ObraFactory.update(ObraAEditar, function(e){
                $scope.ObraAEditar.titulo =$scope.editTitulo;
                $scope.ObraAEditar.ano =$scope.editano;
                $scope.ObraAEditar.descripcion =$scope.editdescripcion;
                $scope.ObraAEditar.usuari_id =$scope.editusuari_id;
                $scope.ObraAEditar.autor_id =$scope.editautor_id;
                $scope.ObraAEditar.epoca_id =$scope.editepoca_id;
                $scope.editTitulo = null;
                $scope.editano = null;
                $scope.editdescripcion = null;
                $scope.editusuari_id = null;
                $scope.editautor_id = null;
                $scope.editepoca_id = null;
                delete $scope.ObraAEditar;
            });
        }
    };
//REVISAR ID!!!
    $scope.eliminarObra = function(obra) {
        console.log(obra._id);
        ObraFactory.delete({id:obra._id},function(l){
            console.log($scope.obres.indexOf(obra));
            $scope.obres.splice($scope.obres.indexOf(obra),1);
        });
        console.log(obra);
    };
});