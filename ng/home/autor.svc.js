// ---- RESOURCE ANGULAR -------
angular.module('didart').factory("HomeFactory", function($resource) {
    return $resource("/api/autors/:id", null,
        {
            'update': { method:'PUT' }
        });
});