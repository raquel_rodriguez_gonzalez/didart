// ------ VARIABLES GLOBALES -----
var express = require("express");
var bodyParser = require("body-parser");
var multer = require("multer");
var app = express();

var port = process.env.PORT || 8080;
app.use(bodyParser.json());

// ----- REFERENCIA A LOS CONTROLADORES -----
app.use("/api",multer( {dest : "./uploads/"}));
app.use("/api/autors", require("./controllers/api/autor"));
app.use("/api/epoques", require("./controllers/api/epoca"));
app.use("/api/obres", require("./controllers/api/obra"));
app.use("/api/usuaris", require("./controllers/api/usuari"));
app.use("/api/imatges", require("./controllers/api/imatge"));

// ----- REFERENCIA A ARCHIVOS EST/ATICOS -----
app.use("/", require("./controllers/static"));

//--------- LISTEN ----------
app.listen(port, function() {
	console.log('server listening on', port);
});