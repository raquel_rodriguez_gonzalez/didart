// ----- VARIABLES GLOBALES -----
var express = require("express");
var router = express.Router();

// ----- VARIABLE RUTA LAYOUTS ----
var options = { root: __dirname + "../../layouts" };



// ---- VARIABLE RUTA ARCHIVOS PUBLICOS
router.use(express.static(__dirname+"/../assets"));
router.use(express.static(__dirname+"/../templates"));

// ---- RUTA A LA PAGINA ------
router.get('/', function(req,res,next){
    res.sendFile("index.html", options);
});
router.get('/paginaprincipal', function(req,res,next){
    res.sendFile("home.html", options);
});
router.get('/login', function(req,res,next){
    res.sendFile("login.html", options);
});
// ---- EXPORTANDO ROUTER ----
module.exports = router;