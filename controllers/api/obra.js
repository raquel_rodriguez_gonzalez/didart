// ---- VARIABLES GLOBALES ----
var router = require("express").Router();
var Obra = require("../../models/obra");
var Imatge = require("../../models/imatge");
var Autor = require("../../models/autor");
var Usuari = require("../../models/usuari");
var path = require('path');


var compressor = path.resolve(__dirname, '../../compressor.js');  // Fitxer que manipules les imatges
function compressAndResize (imageUrl) {
  // Creem un "child process" d'aquesta manera no 
  // fem un bloqueig del EventLoop amb un ús intens de la CPU
  // al processar les imatges
  var childProcess = require('child_process').fork(compressor);
  childProcess.on('message', function(message) {
    console.log(message);
  });
  childProcess.on('error', function(error) {   // Si el procés rep un missatge l'escriurà a la consola
    console.error(error.stack);
  });
  childProcess.on('exit', function() {  //Quan el procés rep l'event exit mostra un missatge a la consola
    console.log('process exited');
  });
  childProcess.send(imageUrl);
}


// CRUD (SQL) = EXPRESS
// CREATE (INSERT) = POST
// READ (SELECT) = GET
// UPDATE (UPDATE) = PUT
// DELETE (DELETE) = DELETE

// ---------------------- GET ------------------
router.get("/", function(req,res, next){
	Obra.find(function(err,obres){
		if(err) {
			return next(err);
		}
		res.json(obres);
	});
});

//------ GET --------- /id/:id -----------------------
router.get("/_id/:id", function(req,res,next){
    console.log(req.params.id);
    Obra.find({"_id": req.params.id}).find(function(err, obra) {
        if (err) {
            return next(err);
        }
        res.json(obra);
    });
});

// --------------------  POST --------------
router.post("/", function (req,res, next) {  
    console.log(req.files);
     var imatge = new Imatge({
        originalName: req.files.foto.originalname,
        name : req.files.foto.name,
        extension: req.files.foto.extension
    });
    Usuari.find({"_id": req.body.usuari_id}).find(function(err, usuari) {
        if (err) {
            return next(err);
        }
        res.json(usuari);
    });
    console.log(imatge);
    imatge.save(function(err,imatge) {
        if (err) return next(err);
        compressAndResize(__dirname+"/../../uploads/" + req.files.foto.name);
        	var obra = new Obra({
                titulo : req.body.titulo,
                ano : req.body.ano,
                descripcion : req.body.descripcion,
                usuari_id : usuari,
                autor_id : req.body.autor_id,
                epoca_id : req.body.epoca_id,
                imagen : imatge._id // Imagen es el Id del Input en el HTML
	    });
	obra.save(function(err, obra){
		if(err) {return next(err)}
		res.status(201).json(obra);
	});
});

//------ DELETE --------- /:id -----------------------
router.delete("/:id", function(req, res, next) {
    console.log(req.params);
    console.log(req.body);
    Obra.remove({"_id": req.params.id}, function(err) {
        if (err) {
            return next(err);
        }
        res.json({"missatge": "Obra amb id " + req.params.id + " esborrada"});
    });
});

// ---------------- PUT ---------
router.put("/", function(req, res,next) {
    console.log(req.body._id);

    Obra.findByIdAndUpdate(req.body._id,req.body,function(err, obra) {
        if(err) {
            return next(err);
        }
        res.json({"missatge": "Obra modificada"});
    });
});
});
module.exports = router;