// ------ VARIABLES GLOBALS ------
var router = require("express").Router();
var Usuari = require("../../models/usuari");
// CRUD (SQL) = EXPRESS
// CREATE (INSERT) = POST
// READ (SELECT) = GET
// UPDATE (UPDATE) = PUT
// DELETE (DELETE) = DELETE

// ---------------------- GET ------------------
router.get("/", function(req,res, next){
	Usuari.find(function(err,usuaris){
		if(err) {
			return next(err);
		}
		res.json(usuaris);
	});
});

//------ GET --------- /id/:id -----------------------
router.get("/_id/:id", function(req,res,next){
    console.log(req.params.id);
    Usuari.find({"_id": req.params.id}).find(function(err, usuari) {
        if (err) {
            return next(err);
        }
        res.json(usuari);
    });
});
// --------------------  POST --------------

router.post("/", function (req,res, next) {
	var usuari = new Usuari({
        // TIENES QUE INDICAR LOS CAMPOS A ESCRIBIR EN POST
        // EJEMPLO: id_usuari : req.body.id_usuari
        // PARA SABER LOS CAMPOS MIRA LOS MODELOS!
        usuario : req.body.usuario,
        contrasenya : req.body.contrasenya,
        email : req.body.email,
        nombre : req.body.nombre,
        fecha : req.body.fecha,
        tipo : req.body.tipo
	});
	usuari.save(function(err, usuari){
		if(err) {return next(err)}
		res.status(201).json(usuari);
	});
});

//------ DELETE --------- /:id -----------------------
router.delete("/:id", function(req, res, next) {
    console.log(req.params);
    console.log(req.body);
    Usuari.remove({"_id": req.params.id}, function(err) {
        if (err) {
            return next(err);
        }
        res.json({"missatge": "Usuari amb id " + req.params.id + " esborrat"});
    });
});

// ---------------- PUT ---------
router.put("/", function(req, res,next) {
    console.log(req.body._id);

    Usuari.findByIdAndUpdate(req.body._id,req.body,function(err, usuari) {
        if(err) {
            return next(err);
        }
        res.json({"missatge": "Usuari modificat"});
    });
});
module.exports = router;