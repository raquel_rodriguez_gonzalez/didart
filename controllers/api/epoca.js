// ----- VARIABLES GLOBALES -------
var router = require("express").Router();
var Epoca = require("../../models/epoca");
// CRUD (SQL) = EXPRESS
// CREATE (INSERT) = POST
// READ (SELECT) = GET
// UPDATE (UPDATE) = PUT
// DELETE (DELETE) = DELETE

// ------------- GET ---------------------
router.get("/", function(req,res, next){
	Epoca.find(function(err,epoques){
		if(err) {
			return next(err);
		}
		res.json(epoques);
	});
});

//------ GET --------- /id/:id -----------------------
router.get("/_id/:id", function(req,res,next){
    console.log(req.params.id);
    Epoca.find({"_id": req.params.id}).find(function(err, epoca) {
        if (err) {
            return next(err);
        }
        res.json(epoca);
    });
});

//-------------- POST --------------------
router.post("/", function (req,res, next) {
	var epoca = new Epoca({
		nombre : req.body.nombre,
		lapso_tiempo : req.body.lapso_tiempo
		
        // TIENES QUE INDICAR LOS CAMPOS A ESCRIBIR EN POST
        // EJEMPLO: id_epoca : req.body.id_epoca
        // PARA SABER LOS CAMPOS MIRA LOS MODELOS!
	});
	epoca.save(function(err, epoca){
		if(err) {return next(err)}
		res.status(201).json(epoca);
	});
});

//------ DELETE --------- /:id -----------------------
router.delete("/:id", function(req, res, next) {
    console.log(req.params);
    console.log(req.body);
    Epoca.remove({"_id": req.params.id}, function(err) {
        if (err) {
            return next(err);
        }
        res.json({"missatge": "Epoca amb id " + req.params.id + " esborrat"});
    });
});

// ---------------- PUT ---------
router.put("/", function(req, res,next) {
    console.log(req.body._id);

    Epoca.findByIdAndUpdate(req.body._id,req.body,function(err, epoca) {
        if(err) {
            return next(err);
        }
        res.json({"missatge": "Epoca modificada"});
    });
});
module.exports = router;