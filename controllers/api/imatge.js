var Imatge = require("../../models/imatge");
var router = require("express").Router();

router.get("/", function(req, res, next) {
    Imatge.find(function(err,imatges) {
        if (err) return next(err);
        res.status(201).json(imatges);
    });
});


router.get("/:grandaria/:id", function(req, res, next) {
    // Aquest get ens donarà una imatge segons la grandària i el id de la imatge
    Imatge.findById(req.params.id, function(err,imatge) {
        if (err) return next(err);
        var options = {
            root: __dirname + '/../../images/'+req.params.grandaria+'/',
            dotfiles: 'deny',
            headers: {
                'x-timestamp': Date.now(),
                'x-sent': true
            }
        };
       
        var fileName = imatge.name;
        res.sendFile(fileName, options, function (err) {
            if (err) {
              console.log(err);
              res.status(err.status).end();
            }
            else {
              console.log('Enviat:', fileName);
            }
          });
    });
})

module.exports = router;