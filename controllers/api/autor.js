// --- VARIABLES GLOBALES ----
var router = require("express").Router();
var Autor = require("../../models/autor");
var Imatge = require("../../models/imatge");
var path = require('path');

var compressor = path.resolve(__dirname, '../../compressor.js');  // Fitxer que manipules les imatges
function compressAndResize (imageUrl) {
  // Creem un "child process" d'aquesta manera no 
  // fem un bloqueig del EventLoop amb un ús intens de la CPU
  // al processar les imatges
  var childProcess = require('child_process').fork(compressor);
  childProcess.on('message', function(message) {
    console.log(message);
  });
  childProcess.on('error', function(error) {   // Si el procés rep un missatge l'escriurà a la consola
    console.error(error.stack);
  });
  childProcess.on('exit', function() {  //Quan el procés rep l'event exit mostra un missatge a la consola
    console.log('process exited');
  });
  childProcess.send(imageUrl);
}


// CRUD (SQL) = EXPRESS
// CREATE (INSERT) = POST
// READ (SELECT) = GET
// UPDATE (UPDATE) = PUT
// DELETE (DELETE) = DELETE
//------------ GET ---------------------
router.get("/", function(req,res, next){
	Autor.find(function(err,autors){
		if(err) {
			return next(err);
		}
		res.json(autors);
	});
});

//------ GET --------- /id/:id -----------------------
router.get("/_id/:id", function(req,res,next){
    console.log(req.params.id);
    Autor.find({"_id": req.params.id}).find(function(err, autor) {
        if (err) {
            return next(err);
        }
        res.json(autor);
    });
});

//------------------ POST ----------------------
router.post("/", function (req,res, next) {
    console.log(req.files);
     var imatge = new Imatge({
        originalName: req.files.foto.originalname,
        name : req.files.foto.name,
        extension: req.files.foto.extension
    });
    console.log(imatge);
    imatge.save(function(err,imatge) {
        if (err) return next(err);
        compressAndResize(__dirname+"/../../uploads/" + req.files.foto.name);
            var autor = new Autor({
            nombre : req.body.nombre,
            nacionalidad : req.body.nacionalidad,
            lapso_vida : req.body.lapso_vida,
            imagen : imatge._id // Imagen es el Id del Input en el HTML
    	});
        	autor.save(function(err, autor){
        		if(err) {return next(err)}
        		res.status(201).json(autor);
        	});
        
        
        //res.status(201).json(imatge);
    });
	
});


//------ DELETE --------- /:id -----------------------
router.delete("/:id", function(req, res, next) {
    console.log(req.params);
    console.log(req.body);
    Autor.remove({"_id": req.params.id}, function(err) {
        if (err) {
            return next(err);
        }
        res.json({"missatge": "Autor amb id " + req.params.id + " esborrat"});
    });
});

// ---------------- PUT ---------
router.put("/", function(req, res,next) {
    console.log(req.body._id);

    Autor.findByIdAndUpdate(req.body._id,req.body,function(err, autor) {
        if(err) {
            return next(err);
        }
        res.json({"missatge": "Autor modificat"});
    });
});
module.exports = router;