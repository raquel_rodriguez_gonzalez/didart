var db = require("../db");
var Usuari = db.model('usuari',{
    usuario: {
        type: String,
        required: true
    },
    contrasenya: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    nombre: {
        type: String,
        required: true
    },
    fecha: {
        type: Date,
        required: true
    },
    tipo: {
        type: String,
        required: true
    }
});
module.exports = Usuari;