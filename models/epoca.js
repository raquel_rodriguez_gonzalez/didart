var db = require("../db");
//var Autor = require("autor");
//var Obra = require("obra");
var Epoca = db.model('Epoca', {
    nombre: {
        type: String,
        required: true
    },
    lapso_tiempo: {
        type: String,
        required: true
    }
});

//module.exports = Autor;
//module.exports = Obra;
module.exports = Epoca;