var db = require("../db");
var Autor = db.model('Autor', {
   nombre: {
       type: String,
       required: true
    },
    nacionalidad: {
        type: String,
        required: true
    },
    lapso_vida: {
        type: String,
        required: true
    },
    imagen: {
        type: db.Schema.Types.ObjectId,
        ref: 'Imatge',
        required: true
    }
});
module.exports = Autor;