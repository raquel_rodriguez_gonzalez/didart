var db = require("../db");
//var Usuari = require("usuari");
//var Autor = require("autor");
var Obra = db.model('Obra', {
    titulo: {
        type: String,
        required: true
    },
    ano: {
        type: String,
        required: true
    },
    descripcion: {
        type:String,
        required:true
    },
    usuari_id: {
        type: db.Schema.Types.ObjectId,
        ref: 'Usuari',
        required: true
    },
    autor_id: {
        type: db.Schema.Types.ObjectId,
        ref : 'Autor',
        required: true
    },
    epoca_id: {
        type: db.Schema.Types.ObjectId,
        ref : 'Epoca',
        required: true
    },
    imagen: {
        type: db.Schema.Types.ObjectId,
        ref : 'Imatge',
        required: true
    }
});

//module.exports = Autor;
//module.exports = Usuari;
module.exports = Obra;