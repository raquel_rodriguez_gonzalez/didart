var db = require("../db");

var schema = new db.Schema({
    originalName: {
        type: String,
        required: true
    },
    name:  {
        type: String,
        required: true
    },
    extension:  {
        type: String,
        required: true
    }
});

var Imatge = db.model("Imatge", schema);
module.exports = Imatge;